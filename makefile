default:
	$(MAKE) -C code

.PHONY:clean 

clean:
	$(RM) *~ *.d* *.o* *# *.core
	$(RM) -r install
	$(MAKE) clean -C code
	$(MAKE) clean -C scripts
	$(MAKE) clean -C subtree_libs
	$(MAKE) clean -C configs
