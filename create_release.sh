#!/bin/bash

set -e # exit on error

binary_name=actioncamstreamer
install_folder=install
enc_password=Hl4?8Q@@#jio

# Product version options are "lite" and "premium".
product_version=$1

# build
if [ "$product_version" = "lite" ]; then
    make RELEASE=1
else if [ "$product_version" = "premium" ]; then
	 make RELEASE=1 PREMIUM=1
     else if [ "$product_version" = "free_premium" ]; then
	      make RELEASE=1 PREMIUM=1 FREE=1
	  else
	      echo "./create_release <product type>"
	      echo "<product type> - premium or lite"
	      exit 0
	  fi
     fi
fi

# create release file folder
if [ ! -e "$install_folder" ]
   then
       mkdir $install_folder
fi

cd $install_folder
cp ../code/$binary_name .

# package/compress
zip_filename=firmware.zip
zip -r $zip_filename $binary_name

# encrypt binary
version=$(git describe --always --tags)
date=$(git log -1 --date=format:'%Y-%m-%d_%H-%M-%S' --pretty=format:%cd)
gpg2 --passphrase $enc_password --batch --yes --output ${date}_GoPie_${product_version}_firmware_$version -c $zip_filename

# checksum
echo $(sha256sum ${date}_GoPie_${product_version}_firmware_$version) > checksum.txt
