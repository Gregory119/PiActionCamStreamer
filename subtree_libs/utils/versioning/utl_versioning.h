#ifndef UTL_VERSIONING_H
#define UTL_VERSIONING_H

namespace UTIL
{
  const char * const GetVersion();
};

#endif
