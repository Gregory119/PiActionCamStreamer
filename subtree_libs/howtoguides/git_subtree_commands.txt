==================================================
Add a subtree
==================================================
- First add a remote repo
$ git remote add subtree_remote <repo url>
Replace subtree_remote with a preferred name.

- from the top directory
$ git subtree add --prefix subtree_remote <ref> --squash
<sub tree dir> is the sub directory that will be created in current directory, which will contain the remote repo files as a subtree.
<ref> A ref is anything pointing to a commit, for example, branches (heads), tags, and remote branches.
--squash Squashes all the remote repo commits into a single imported commit.

==================================================
Committing to a subtree
==================================================
Use standard commit syntax. Commit subtree files separately for clarity, such that they make sense when removed from the context of the repository they have been added to.

==================================================
Pull a subtree
==================================================
- from the top directory
git subtree pull --prefix subtree_remote <ref> --squash

==================================================
Push a subtree
==================================================
- from the top directory
git subtree push --prefix subtree_remote <ref>
- only commits that include files in the subtree are pushed to the repo of the subtree.

References:
https://medium.com/@v/git-subtrees-a-tutorial-6ff568381844
https://www.atlassian.com/blog/git/alternatives-to-git-submodule-git-subtree
https://git-scm.com/book/en/v1/Git-Tools-Subtree-Merging
